package api;

import com.fasterxml.jackson.databind.ObjectMapper;
import dto.FreeNewsDTO;
import dto.NewsDTO;
import lombok.Getter;
import lombok.Setter;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class FreeNewsImpl implements TopNews{
    private final ObjectMapper obj = new ObjectMapper();
    private HttpRequest request;
    @Setter@Getter
    private String key;
    @Setter@Getter
    private String host;
    @Override
    public NewsDTO getNews() {
        sendRequest("https://free-news.p.rapidapi.com/v1/search?q=Elon%20Musk&lang=en&page_size=1");
        try {
            HttpResponse<String> response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
            return obj.readValue(response.body(), FreeNewsDTO.class);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String getBanner() {
        return "Free News";
    }

    @Override
    public void sendRequest(String link) {
        request = HttpRequest.newBuilder()
                .uri(URI.create("https://free-news.p.rapidapi.com/v1/search?q=Elon%20Musk&lang=en&page_size=1"))
                .header("x-rapidapi-key", key)
                .header("x-rapidapi-host", "free-news.p.rapidapi.com")
                .method("GET", HttpRequest.BodyPublishers.noBody())
                .build();

    }
}
