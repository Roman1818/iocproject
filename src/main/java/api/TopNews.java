package api;


import dto.NewsDTO;


public interface TopNews {
    NewsDTO getNews();
    String getBanner();
    void sendRequest(String link);

}
