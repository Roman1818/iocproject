package api;


import com.fasterxml.jackson.databind.ObjectMapper;
import dto.HackerNewsDTO;
import dto.NewsDTO;
import lombok.Getter;
import lombok.Setter;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;



public class HackerNewsImpl implements TopNews {

    private final ObjectMapper obj = new ObjectMapper();
    private HttpRequest request;
    @Setter@Getter
    private String key;
    @Setter@Getter
    private String host;



    @Override
    public NewsDTO getNews() {
        sendRequest("https://community-hacker-news-v1.p.rapidapi.com/maxitem.json?print=pretty");

        try {
            HttpResponse<String> response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
            sendRequest("https://community-hacker-news-v1.p.rapidapi.com/item/"+response.body().strip()+".json?print=pretty");
            response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
            return obj.readValue(response.body(), HackerNewsDTO.class);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public String getBanner() {
        return "Hacker News";
    }

    @Override
    public void sendRequest(String link){
        request = HttpRequest.newBuilder()
                .uri(URI.create(link))
                .header("x-rapidapi-key", key)
                .header("x-rapidapi-host", host)
                .method("GET", HttpRequest.BodyPublishers.noBody())
                .build();

    }
}



