package view;

import api.TopNews;


import dto.NewsDTO;
import lombok.RequiredArgsConstructor;


import java.util.Scanner;

@RequiredArgsConstructor
public class View {

    private final TopNews topNews;
    private NewsDTO newsDTO;

    public void run(){
        System.out.println("\t\tWelcome!\nGet the latest news from "+topNews.getBanner()+"\n1 - Get\n2 - Exit");
        Scanner scanner = new Scanner(System.in);
        String choice;
        while (true) {
            System.out.print(">>> ");
            choice = scanner.nextLine();
            if (choice.equals("1")) {
                newsDTO = topNews.getNews();
                show();
            } else if (choice.equals("2")) {
                System.out.println("Thanks, bye!");
                return;
            } else {
                System.out.println("Invalid choice!");
            }
        }

    }


    private void show(){
        if (newsDTO == null){
            System.out.println("API didn't response!");
        }else{
            System.out.println(newsDTO);
        }


    }
}




