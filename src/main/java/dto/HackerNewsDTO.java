package dto;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Setter;
import lombok.experimental.Accessors;



@JsonIgnoreProperties(ignoreUnknown = true)
public class HackerNewsDTO implements NewsDTO{


    @Setter
    private String by;
    @Setter
    private String text;
    @Setter
    private String type;


    @Override
    public String toString() {
        return "by -> " + by +
                "\ntext -> " + text + "\ntype -> " + type;
    }
}
