package dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


import java.util.List;


@JsonIgnoreProperties(ignoreUnknown = true)
public class FreeNewsDTO implements NewsDTO{

@Getter@Setter
private List<Article> articles;


    @Override
    public String toString() {
        return ""+articles;
    }
    @JsonIgnoreProperties(ignoreUnknown = true)
public static class Article{

        @Setter@Getter
    private String title;
        @Setter@Getter
    private String author;
        @Setter@Getter
    private String published_date;
        @Setter@Getter
    private String link;


        @Override
        public String toString() {
            return"title -> " + title+"\nauthor -> " + author +
                    "\npublished_date -> " + published_date +
                    "\nlink -> " + link;
        }
    }
}
