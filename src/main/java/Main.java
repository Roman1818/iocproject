import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import view.View;


public class Main {




    public static void main(String[] args){

        ApplicationContext ctx = new ClassPathXmlApplicationContext("context.xml");
        View view = ctx.getBean(View.class);
        view.run();

    }
}
